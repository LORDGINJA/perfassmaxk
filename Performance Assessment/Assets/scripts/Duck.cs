﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class Duck : MonoBehaviour {
	public float Speed = 0f;
	private float movex = 0f;
	private float movey = 0f;
	public int playerHealth = 100;
	public int damageRecieved = 2;
	public AudioClip footstep2;
	private Animator animator;


	protected void Start(){
		//base.Start ();
		animator = GetComponent<Animator> ();
	}

	private void FixedUpdate () {
		movex = Input.GetAxis ("Horizontal");
		movey = Input.GetAxis ("Vertical");
		GetComponent<Rigidbody2D>().velocity = new Vector2 (movex * Speed, movey * Speed);
		GameController.Instance.healthText.text = "Health: " + playerHealth.ToString();
	}

private void OnTriggerEnter2D(Collider2D objectPlayerCollideWith){
	
	if(objectPlayerCollideWith.tag == "Coins"){
		playerHealth += 2;
		GameController.Instance.healthText.text = "Health: " + playerHealth;
		objectPlayerCollideWith.gameObject.SetActive(false);
		SoundController.Instance.PlaySingle(footstep2);
	}
	
	else if(objectPlayerCollideWith.tag == "Enemy"){
			playerHealth -= 2;
			GameController.Instance.healthText.text =  "Health: " + playerHealth;
			SoundController.Instance.PlaySingle(footstep2);
			animator.SetTrigger ("playerHurt");
	}
}

}