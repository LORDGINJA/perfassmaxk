﻿using UnityEngine;
using System.Collections;

public class BadDuck : MonoBehaviour {
	public int maxSpeed;
	
	private Vector3 startPosition;
	
	void Start () 
	{
		maxSpeed = 3;
		
		startPosition = transform.position;
	}
	
	void Update ()
	{
		MoveVertical ();
	}
	
	void MoveVertical()
	{
		transform.position = new Vector2(transform.position.x, startPosition.y + Mathf.Sin(Time.time * maxSpeed));
		
		if(transform.position.y > 1.0f)
		{
			transform.position = new Vector2(transform.position.x, transform.position.y);
		}
		else if(transform.position.y < -1.0f)
		{
			transform.position = new Vector2(transform.position.x, transform.position.y);
		}
	}

}
